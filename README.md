# DOCUMENTO DE INICIO DE PROYECTO

**Definición del proyecto:**

**Proyecto:** Desarrollo web con consumo de microservicios en tecnologías Django y Spring boot

**Cliente: Empresa “Electrical and Automation Design”**

**Objetivo:** Implementar un componente web que permita la autenticación de usuarios internos de la empresa y la realización de operaciones orientadas a la cotización y facturación de equipos de ingeniería.

**Duración:** Ciclo 4 de MisiónTIC 2021  (Noviembre 02 a Diciembre 12 de 2021)**.**

**Seguimiento y control del proyecto:**

**Metodología:** Scrum

**Software de seguimiento:** Jira

**Repositorio:** Bit bucket

**Equipo de desarrollo:**

Mateo Andrés Rodriguez Pereira

Andrés Potosi

Wendy Maritza Cuy Salcedo

Juan Camilo Acosta

Mauricio Ortega Ortiz

Carlos Eduardo Ramos Escobar


# Ramas

**Main**
Esqueleto proyecto

**auth_app**
Creacion e inicialización de microservicio usuarios. Permite creacion, consulta, autenticacion de token. 

**auth_app_back**
Basada en la rama auth_app, con despliegue del contenedor en Docker y Bitbucket.  Se despliega en Heroku con los respoctivos 
contenedores en Docker. Incluye archivo con colección de Postman. 
Link: https://mision-tic-auth-ms-ciclo-4.herokuapp.com/

**coti-mic**
Creación del segundo microservicio basado en Java y MongoDB. Permite la creación y consulta de cotizaciones. Se despliega en Heroku con los respoctivos 
contenedores en Docker. Incluye archivo con colección de Postman. 
Link: https://cotizaciones-ms-ciclo-4.herokuapp.com/

**API-Gateway**
Creación del API para realizar la integración de los dos microservicios.